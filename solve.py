from abc import ABCMeta
import sys

# vertex
# edge


class TreeMark:
    EMPTY = -1

    def __init__(self, x):
        self.r = x
        self.p = self.EMPTY

    def __str__(self):
        return "(%s, %s)" % (self.r, self.p)

    def __repr__(self):
        return "%s<%s>" % (self.__class__.__name__, self.__str__())


class BaseTree(metaclass=ABCMeta):
    def __init__(self, g: 'Graph'):
        self.g = g

    def is_the_same_subtree(self, i, j):
        raise NotImplementedError

    def join_edge(self, i, j, k):
        raise NotImplementedError

    def split_pop_edge(self):
        raise NotImplementedError

    def count(self):
        raise NotImplementedError

    def __str__(self):
        raise NotImplementedError

    def __repr__(self):
        return "%s<%s>" % (self.__class__.__name__, self.__str__())


class OriginalTree(BaseTree):
    def __init__(self, g: 'Graph'):
        super().__init__(g)

        self.vertices_marks = [TreeMark(i) for i in range(self.g.n)]
        self.ks = []

    def is_the_same_subtree(self, i, j):
        return self.vertices_marks[i].r == self.vertices_marks[j].r

    def count(self):
        return len(self.ks)

    def tree_human(self):
        return list(map(lambda s: [s[0]+1, s[1]+1], self.ks))

    def _B_join_subtrees(self, xa, xb):

        r1 = self.vertices_marks[xa].r
        r2 = self.vertices_marks[xb].r

        if r2 < r1:
            r1, r2 = r2, r1
            xa, xb = xb, xa

        for vm in self.vertices_marks:
            if vm.r == r2:
                vm.r = r1

        i = xb
        ptm = xa
        ntm = TreeMark.EMPTY

        while i != TreeMark.EMPTY:
            ntm = self.vertices_marks[i].p
            self.vertices_marks[i].p = ptm

            ptm = i
            i = ntm

        #assert self.vertices_marks[xb].p == 0, "pb != 0"
        #self.vertices_marks[xb].p = xa

    def _C_split_tree(self, xa, xb):
        pa = self.vertices_marks[xa].p
        pb = self.vertices_marks[xb].p

        if pb == xa:
            # t2
            pass
        elif pa == xb:
            xa, xb = xb, xa
            # t1
        else:
            assert 0, "fail %r " % [xa, self.vertices_marks[xa], xb, self.vertices_marks[xb]]

        s = {xb}
        self.vertices_marks[xb].r = xb  ############
        self.vertices_marks[xb].p = TreeMark.EMPTY

        seen = set()
        while True:
            ns = set()
            for j in range(len(self.vertices_marks)):
                vm = self.vertices_marks[j]
                if vm.p in s and (j not in seen):
                    vm.r = xb
                    ns.add(j)
                    seen.add(j)

            if not ns:
                return

            s |= ns

    def join_edge(self, i, j, k):
        self._B_join_subtrees(i, j)
        self.ks.append((i, j, k,))

    def split_pop_edge(self):
        i, j, k = self.ks.pop()
        self._C_split_tree(i, j)
        return i, j, k

    def __str__(self):
        return "%s, %s" % (self.vertices_marks, self.ks)


class MainMethod:
    TREE = OriginalTree

    def __init__(self, g: 'Graph'):
        self.g = g

        # Step 0
        self.xs = 0
        xs_edges = list(self.g.edges_of(self.xs))
        self.ds = len(xs_edges)
        # new -> old
        self.edge_map = xs_edges + list(self._map_rest_edges(xs_edges, self.g.m))
        assert len(self.edge_map) == self.g.m

        # Step 1
        self.tree = self.TREE(self.g)

        self.k = 0  # todo +1

        self.ii = 0

    @classmethod
    def _map_rest_edges(cls, xs_edges, m):
        s = set(xs_edges)

        for e in range(m):
            if e not in s:
                yield e

    def _get_edge(self, i):
        return self.g.get_edge(self.edge_map[i])

    def _step2(self):
        while True:
            self.ii += 1
            #print(self.k)
            #print(self.tree)
            
            #print(("ee", self.k, self.g.m))

            if self.k >= self.g.m:
                self._step5()
                continue
            else:
                i, j = self._get_edge(self.k)
                ##print((i, j,))
                if self.tree.is_the_same_subtree(i, j):
                    self.k += 1
                else:
                    # Step 3
                    self.tree.join_edge(i, j, self.k)
                    
                    # Step 4
                    if self.tree.count() >= self.g.n - 1:  ##########
                        print(self.tree.tree_human())
                        self._step5()
                    else:
                        self.k += 1

    def _step5(self):

        i, j, l = self.tree.split_pop_edge()

        self.k = l + 1

        #print([l + 1, self.ds, l + 1 - self.ds])

    def run(self):
        try:
            self._step2()
        except IndexError:
            pass

        print(self.ii, file=sys.stderr)


class BaseGraphStorage(metaclass=ABCMeta):

    def __init__(self, n):
        self.n = n

    def add_edge(self, a, b):
        raise NotImplementedError

    def edges_count(self):
        raise NotImplementedError

    def get_edge(self, i):
        raise NotImplementedError

    def edges(self):
        raise NotImplementedError

    def edges_of(self, e):
        raise NotImplementedError



class EdgelistGraphStorage(BaseGraphStorage):
    def __init__(self, n):
        super().__init__(n)
        self.en = []

    def add_edge(self, a, b):
        if b < a:
            a, b = b, a

        self.en.append((a, b,))

    def edges_count(self):
        return len(self.en)

    def get_edge(self, i):
        return self.en[i]

    def edges(self):
        return self.en

    def edges_of(self, e):
        for i in range(self.edges_count()):
            a, b = self.get_edge(i)
            if a == e or b == e:
                yield i


class Graph:
    def __init__(self, n, storage: BaseGraphStorage=EdgelistGraphStorage):
        self.n = n
        self._storage = storage(n)

    def add_edge(self, a, b):
        self._storage.add_edge(a, b)

    def edges(self):
        return self._storage.edges()

    def get_edge(self, i):
        return self._storage.get_edge(i)

    def edges_of(self, e):
        return self._storage.edges_of(e)

    @property
    def m(self):
        return self._storage.edges_count()
    


def main():
    n = int(input())
    k = int(input())

    g = Graph(n)
    for _ in range(k):
        a, b = [int(s) for s in (x.strip() for x in input().split(" ")) if s]
        g.add_edge(a-1, b-1)

    MainMethod(g).run()

"""
4
5
1 2
2 3
3 4
4 1
2 4
"""

"""
5
7
1 2
1 3
2 3
2 5
2 4
3 4
4 5
"""


if __name__ == "__main__":
    main()